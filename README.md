# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Version :**

Une seul version du compilateur la dernière qui est commit (les anciens commit pas présent du au fait que j'ai du recréer un projet pour push comme préciser dans le mail.)

**This version Can handle :**

// Program := [VarDeclarationPart] StatementPart
// Declaration := Ident {"," Ident} ":" Type
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement|DisplayStatement|....
// AssignementStatement := Identifier ":=" Expression
// DisplayStatement := "DISPLAY" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

// IfStatement := "If" Expression "Then" Statement ["ELSE" Statement]
// WhileStatement := "While" Expression "Do" Statement
// ForStatement := "For" ID ":=" Expression ("To"|"DownTo") Expression "Do" Statement
// BlockStatement := "Begin" Statement {";" Statement} "End"

**News**

Les 2 seuls implémentation qui ne sont pas dans la correction sont la fonction ForStatement comprenant le To et le DownTo dans le tp4.

Je n'ai pas pu compiler et donc vérifier si il y avait des erreurs car la compilation ne fonctionne pas de mon pc donc j'ai juste fais ce que j'ai pu.