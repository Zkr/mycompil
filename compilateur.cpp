#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, CHAR, DOUBLE};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum TYPES> DeclaredVariables;

unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
enum TYPES Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

enum TYPES Number(void){
	bool is_a_decimal=false;
	double d;					
	unsigned int *i;			
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){
		d=atof(lexer->YYText());
		i=(unsigned int *) &d;
		cout <<"\t subq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\t movl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\t movl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

enum TYPES CharConst(void){
	cout<<"\t movq $0, %rax"<<endl;
	cout<<"\t movb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\t push %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}



enum TYPES Expression(void);			// Called by Term() and calls Term()
void Statement(void);
enum TYPES AssignementStatement(void); 

enum TYPES IfStatement(void){
	TYPES type8;
	if(strcmp(lexer->YYText(), "If" )==0){
		current=(TOKEN) lexer->yylex();
		type8 = Expression();
		if (type8 != BOOLEAN){
			Error("l'expression n'est pas de type BOOLEAN");
		}
		cout << "\t pop %rax "<<endl;
		cout << "\t cmpq $0, %rax "<<endl;
		cout << "\t je ElseIF "<<endl;
		cout << "\t ThenIF: "<<endl;
		if(strcmp(lexer->YYText(), "Then" )==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\t jmp SuiteIF "<<endl;
			cout << "\t ElseIF: "<<endl;
			if(strcmp(lexer->YYText(), "Else" )==0){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else{
			Error("Then attendu");
		}
	}
	else{
		Error("If attendu");
	}
	cout << "\t SuiteIF:"<<endl;
}

enum TYPES WhileStatement(void){
	TYPES type9;
	if(strcmp(lexer->YYText(), "While" )==0){
		current=(TOKEN) lexer->yylex();
		type9=Expression();
		if (type9!=BOOLEAN){
			Error("l'expression n'est pas de type BOOLEAN");
		}
		cout << "\t pop %rax "<<endl;
		cout << "\t cmpq $0, %rax "<<endl;
		cout << "\t je SuiteWHILE "<<endl;
		cout << "\t DoWHILE: "<<endl;
		if(strcmp(lexer->YYText(), "Do" )==0){
		current=(TOKEN) lexer->yylex();
		Statement();
		}
		else{
			Error("Do attendu");
		}
	}
	else{
		Error("While attendu");
	}
	cout << "\t SuiteWHILE:"<<endl;
}

void ForStatement(void){
	int myTag = ++TagNumber;
	if(strcmp(lexer->YYText(), "For" )==0){
		current=(TOKEN) lexer->yylex();
		string variable = AssignementStatement();
		if(strcmp(lexer->YYText(), "To" )==0){
			current=(TOKEN) lexer->yylex();
			cout << "\t toDO: "<<endl;
			Expression();
			cout << "\t pop %rax "<<endl;
			cout << "\t cmpq $0, %rax "<<endl;
			cout << "\t je SuiteFOR "<<myTag<<endl;
			if(strcmp(lexer->YYText(), "Do" )==0){
				current=(TOKEN) lexer->yylex();
				Statement();
				cout << "\t addq $1,"<<variable<<endl;
				cout << "\t jmp toDO"<<myTag<<endl;
				
			}
			else{
				Error("Do attendu");
			}
		}
		else if(strcmp(lexer->YYText(), "DownTo" )==0){
			current=(TOKEN) lexer->yylex();
			cout << "\t toDO: "<<endl;
			Expression();
			cout << "\t pop %rax "<<endl;
			cout << "\t cmpq $0, %rax "<<endl;
			cout << "\t je SuiteFOR "<<myTag<<endl;
			if(strcmp(lexer->YYText(), "Do" )==0){
				current=(TOKEN) lexer->yylex();
				Statement();
				cout << "\t subq $1,"<<variable<<endl;
				cout << "\t jmp toDO"<<myTag<<endl;
				
			}
			else{
				Error("Do attendu");
			}
		}
		else{
			Error("DOWNTO ou TO attendu");
		}
	}
	else{
		Error("For attendu");
	}
	cout << "\t SuiteFOR"<<myTag<<":"<<endl;
}

void BlockStatement(void){
	current=(TOKEN) lexer->yylex();
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	};
	if(current!=KEYWORD||strcmp(lexer->YYText(), "End")!=0){
		Error("End attendu");
	}
	current=(TOKEN) lexer->yylex();  
}

void Display(void)
{
    if(strcmp(lexer->YYText(), "Display" )==0){
	{
		current=(TOKEN) lexer->yylex();
        if(Expression()==INTEGER)
        {
            cout << "\t pop %rdx\t# The value to be displayed"<<endl;
            cout << "\t movq $FormatString1, %rsi\t#\"%llu\\n\""<<endl;
            cout << "\t movl	$1, %edi"<<endl;
            cout << "\t movl	$0, %eax"<<endl;
            cout << "\t call	__printf_chk@PLT"<<endl;

        }
        else{Error("Integer attendu");}  
    }
}

enum TYPES Factor(void){
	enum TYPES type;
	switch(current){
		case RPARENT:
			current=(TOKEN) lexer->yylex();
			type=Expression();
			if(current!=LPARENT)
				Error("')' était attendu");		// ")" expected
			else
				current=(TOKEN) lexer->yylex();
			break;
		case NUMBER:
			type=Number();
			break;
		case ID:
			type=Identifier();
			break;
		case CHARCONST:
			type=CharConst();
			break;
		default:
			Error("'(', ou constante ou variable attendue.");
	};
	return type;
}


// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void){
	TYPES type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		switch(mulop){
			case AND:
				if (type2!=BOOLEAN){
					Error("Pas le même type");
				}
				cout << "\t pop %rbx"<<endl;	// get first operand
				cout << "\t pop %rax"<<endl;	// get second operand
				cout << "\t mulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\t push %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(type2==INTEGER){
					cout << "\t pop %rbx"<<endl;	// get first operand
					cout << "\t pop %rax"<<endl;	// get second operand
					cout << "\t mulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\t push %rax\t# MUL"<<endl;	// store result
				}
				else{
					cout<<"\t fldl	8(%rsp)\t"<<endl;
					cout<<"\t fldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\t fmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\t fstpl 8(%rsp)"<<endl;
					cout<<"\t addq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;

			case DIV:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la division");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;

			case MOD:
				if (type2!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	TYPES type3, type4;
	OPADD adop;
	type3=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type4=Term();
		if(type3!=type4){
			Error("type différent")
		}
		switch(adop){
			case OR:
				if (type4!=BOOLEAN){
					Error("Pas le même type");
				}
				cout << "\t pop %rbx"<<endl;	// get first operand
				cout << "\t pop %rax"<<endl;	// get second operand
				cout << "\t orq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\t push %rax"<<endl;			// store result
				break;			
			case ADD:
				if(type2!=INTEGER&&type2!=DOUBLE){
					Error("opérande non numérique pour l'addition");
				}
				if(type2==INTEGER){
					cout << "\t pop %rbx"<<endl;	// get first operand
					cout << "\t pop %rax"<<endl;	// get second operand
					cout << "\t addq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\t push %rax"<<endl;			// store result
				}
				else{
					cout<<"\t fldl	8(%rsp)\t"<<endl;
					cout<<"\t fldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\t faddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\t fstpl 8(%rsp)"<<endl;
					cout<<"\t addq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;			
			case SUB:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("opérande non numérique pour la soustraction");
				if(type2==INTEGER){
					cout << "\t pop %rbx"<<endl;	// get first operand
					cout << "\t pop %rax"<<endl;	// get second operand
					cout << "\t subq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\t push %rax"<<endl;			// store result
				}
				else{
					cout<<"\t fldl	(%rsp)\t"<<endl;
					cout<<"\t fldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\t fsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\t fstpl 8(%rsp)"<<endl;
					cout<<"\t addq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	

			default:
				Error("opérateur additif inconnu");
		}
	}
	return type3;
}

enum TYPES Type(void){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}

	else
		Error("type inconnu");
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> idents;
	enum TYPES type;
	if(current!=ID){
		Error("id attendu");
	}
	idents.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID){
			Error("id attendu");
		}
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON){
		Error("caractère ':' attendu");
	}
	current=(TOKEN) lexer->yylex();
	type=Type();
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it){
	    switch(type){
			case BOOLEAN:
			case INTEGER:
				cout << *it << ":\t.quad 0"<<endl;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0"<<endl;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0"<<endl;
				break;
			default:
				Error("type inconnu.");
		};
		DeclaredVariables[*it]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT){
		Error("'.' attendu");
	}
	current=(TOKEN) lexer->yylex();
}



// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	TYPES type5;
	OPREL oprel;
	type5=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type5=SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		if(type1!=DOUBLE){
			cout << "\t pop %rax"<<endl;
			cout << "\t pop %rbx"<<endl;
			cout << "\t cmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\t fldl	(%rsp)\t"<<endl;
			cout<<"\t fldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\t fcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\t faddp %st(1)\t# pop nothing"<<endl;
		}

		switch(oprel){
			case EQU:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		type5=BOOLEAN;
	}
	return type5;
}

// AssignementStatement := Identifier ":=" Expression
TYPES AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	TYPES type6 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	TYPES type7 = Expression();
	cout << "\tpop "<<variable<<endl;
	if (type6!=type7){
		Error("pas du même type");
	}
	if (type6==CHAR){		
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else{
		cout << "\tpop "<<variable<<endl;
	}	
}

// Statement := AssignementStatement
void Statement(void){
	if(strcmp(lexer->YYText(), "If" )==0){
		IfStatement();
	}
	else if(strcmp(lexer->YYText(), "While" )==0){
		WhileStatement(); 
	}
	else if(strcmp(lexer->YYText(), "For" )==0){
		ForStatement(); 
	}
	else if(strcmp(lexer->YYText(),"Begin")==0){
		BlockStatement();
	}
	else if(strcmp(lexer->YYText(),"Display")==0){
		Display();
	}
	else{
		AssignementStatement();
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0){
		VarDeclarationPart();
	}
	StatementPart();	
}

int main(void){
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	current=(TOKEN) lexer->yylex();
	Program();
	cout << "FormatString2:\t.string \"%lf\\n\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\\n\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





